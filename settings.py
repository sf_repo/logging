LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'style': '{',
    'formatters': {
        'debug': {
            'format': '[{server_time}] {levelname} {message}',
        },
        'warning': {
            'format': '[{server_time}] {levelname} {message} {pathname}',
        },
        'errcrit': {
            'format': '[{server_time}] {levelname} {message} {pathname} {exc_info}',
        },
        'file': {
            'format': '[{server_time}] {levelname} {module} {message}',
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console1': {
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'debug',
        },
        'console2': {
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'level': 'WARNING',
            'formatter': 'warning',
        },
        'console3': {
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'level': 'ERROR',
            'formatter': 'errcrit',
        },
        'file1': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'general.log',
            'formatter': 'file',           
        },
        'file2': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': 'errors.log',
            'formatter': 'errcrit',           
        },
        'file3': {
            'class': 'logging.FileHandler',
            'filename': 'security.log',
            'formatter': 'file',           
        },
        'mail': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'formatter': 'warning',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console1', 'console2', 'console3', 'file1'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['file2', 'mail'],
            'propagate': False,
        },
        'django.server': {
            'handlers': ['file2', 'mail'],
            'propagate': False,
        },
        'django.template': {
            'handlers': ['file2'],
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['file2'],
            'propagate': False,
        },
        'django.security': {
            'handlers': ['file3'],
            'propagate': False,
        },
    },
}
